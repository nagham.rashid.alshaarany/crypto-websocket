import { Select } from "antd";
import React from "react";
import './SelectProduct.scss'
const { Option } = Select;

interface SelectProductProps {
    onProductChange: (product: string) => void;
}

const options = [
    { value: 'BTC-USD', label: 'BTC-USD' },
    { value: 'ETH-USD', label: 'ETH-USD' },
    { value: 'LTC-USD', label: 'LTC-USD' },
    { value: 'BCH-USD', label: 'BCH-USD' },
]

export default function SelectProduct({ onProductChange }: SelectProductProps) {

    return (
        <div className="select-product">
            <h3>Select currency pair</h3>
            <Select
                style={{ width: 120 }}
                onChange={(key) => onProductChange(key)}
                options={options}
                defaultValue={'BTC-USD'}
            >
                {options.map(option => (
                    <Option key={option.value} value={option.value}>
                        {option.label}
                    </Option>
                ))}
            </Select>
        </div>

    )
}