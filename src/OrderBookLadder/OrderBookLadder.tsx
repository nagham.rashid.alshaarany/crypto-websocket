import React, { useEffect, useState } from 'react';
import { Select, Table } from 'antd';
import type { TableColumnsType } from 'antd';
import { TradesData } from '../Services/TradesHook';
import './OrderBookLadder.scss'
const { Option } = Select;

interface DataType {
    key: React.Key;
    marketSize: number;
    price: number;
    mySize: string;
}

interface LadderProp {
    trades: TradesData;
    onAggregationChange: (agg: number) => void;
}
const columns: TableColumnsType<DataType> = [
    {
        title: 'Market Size',
        dataIndex: 'marketSize',
    },
    {
        title: 'Price (USD)',
        dataIndex: 'price',
    },
    {
        title: 'My Size',
        dataIndex: 'mySize',
    },
];
const columns2: TableColumnsType<DataType> = [
    {
        dataIndex: 'marketSize',
    },
    {
        dataIndex: 'price',
    },
    {
        dataIndex: 'mySize',
    },
];
const aggregations = [
    { value: 0.01, label: '0.01' },
    { value: 0.05, label: '0.05' },
    { value: 0.10, label: '0.10' },
    { value: 100, label: '100' },

]

export default function OrderBookLadder({ trades, onAggregationChange }: LadderProp) {
    const [buy, setBuyOrders] = useState<DataType[]>([]);
    const [sell, setSellOrders] = useState<DataType[]>([]);

    useEffect(() => {
        const newBuyOrders = Array.from(trades.orderBook.buy)
            .filter(item => !isNaN(item[0]))
            .map((item, index) => ({
                key: index,
                marketSize: item[1],
                price: item[0],
                mySize: "-"
            }));

        setBuyOrders(prevBuyOrders => {
            const updatedOrders = [...prevBuyOrders, ...newBuyOrders].slice(-7);
            return updatedOrders.map((order, index) => ({ ...order, key: index }));
        });

        const newSellOrder = Array.from(trades.orderBook.sell)
            .filter(item => !isNaN(item[0]))
            .map((item, index) => ({
                key: index,
                marketSize: item[1],
                price: item[0],
                mySize: "-"
            }));

        setSellOrders(prevSellOrders => {
            const updatedOrders = [...prevSellOrders, ...newSellOrder].slice(-7);
            return updatedOrders.map((order, index) => ({ ...order, key: index }));
        });
    })

    return (
        <div className='ladder'>
            <h2>Order Book</h2>
            <div className='sell'>
                <Table columns={columns} dataSource={sell} pagination={false} tableLayout='fixed' rowHoverable={false} />

            </div>
            <div className='aggregation-bar'>
                <span>USD Spread</span>
                <Select options={aggregations} defaultValue={100} onChange={(key) => onAggregationChange(key)}>
                    {aggregations.map(agg => (
                        <Option key={agg.value} value={agg.value}>
                            {agg.label}
                        </Option>
                    ))}
                </Select>
            </div>
            <div className='buy'>
                <Table columns={columns2} dataSource={buy} pagination={false} tableLayout='fixed' rowHoverable={false} />

            </div>

        </div>
    );
};

