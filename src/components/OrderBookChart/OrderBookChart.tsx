import ReactApexChart from "react-apexcharts";
import './OrderBookChart.scss'
import { TradesData } from "../../Services/TradesHook";


export default function OrderBookChart({ trades }: { trades: TradesData }) {

    const chartOption: ApexCharts.ApexOptions = {
        series: [
            { name: 'Bids', data: trades.bidsSeries },
            { name: 'Asks', data: trades.asksSeries }],
        chart: {
            type: 'line',
            height: 350,
            animations: {
                enabled: true,
                easing: 'linear',
                dynamicAnimation: {
                    speed: 200
                }
            },
        },
        xaxis: {
            type: 'datetime'
        },
        yaxis: {
            title: {
                text: 'Price'
            }
        },
        tooltip: {
            x: {
                format: 'dd MMM yyyy HH:mm:ss'
            }
        },
        colors: ['#00A6ED', '#FFA500']
    };

    return (

        <div className='chart-container'>
            <h1>Real Time Chart</h1>
            <div className='bests'>
                <div className='best' >
                    <h2 className='bid'>Best Bid: itbit</h2>
                    <div className='amounts'>
                        <div className='price'>
                            <span className='amount'>{trades.bestBid?.price}</span>
                            <span>Bid Price</span>
                        </div>
                        <div className='size'>
                            <span className='amount'>{trades.bestBid?.size}</span>
                            <span>Bid Quantity</span>
                        </div>
                    </div>

                </div>
                <div className='best' >
                    <h2 className='ask'>Best Ask: kraken</h2>
                    <div className='amounts'>
                        <div className='price'>
                            <span className='amount'>{trades.bestAsk?.price}</span>
                            <span>Ask Price</span>
                        </div>
                        <div className='size'>
                            <span className='amount'>{trades.bestAsk?.size}</span>
                            <span>Ask Quantity</span>
                        </div>
                    </div>
                </div>
            </div>
            <ReactApexChart options={chartOption} series={chartOption.series} type="line" height={350} />

        </div>
    );
};