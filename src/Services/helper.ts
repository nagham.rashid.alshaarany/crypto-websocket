export function isTimestampWithinRange(targetTimestamp: string, startTimestamp?: string, endTimestamp?: string): boolean {
    const targetDate = new Date(targetTimestamp);

    // Check if the target date is valid
    if (isNaN(targetDate.getTime())) {
        throw new Error('Invalid target date format.');
    }

    let startDate: Date | null = null;
    let endDate: Date | null = null;

    if (startTimestamp !== undefined) {
        startDate = new Date(startTimestamp);
        if (isNaN(startDate.getTime())) {
            throw new Error('Invalid start date format.');
        }
    }

    if (endTimestamp !== undefined) {
        endDate = new Date(endTimestamp);
        if (isNaN(endDate.getTime())) {
            throw new Error('Invalid end date format.');
        }
    }

    // Check if the target date is within the range
    if (startDate !== null && endDate !== null) {
        return targetDate >= startDate && targetDate <= endDate;
    } else if (startDate !== null) {
        return targetDate >= startDate;
    } else if (endDate !== null) {
        return targetDate <= endDate;
    } else {
        // If both start and end dates are undefined, consider it an unbounded range
        return true;
    }
}