import { useState, useEffect, useRef } from "react";
import useCoinbaseWebSocket, { OrderBookRecord, Point } from "./WebSocketHook";

export interface TradePoint {
  x: string;
  y: number;
}

export interface TradesData {
  bidsSeries: TradePoint[];
  asksSeries: TradePoint[];
  bestBid?: { price: number; size: number };
  bestAsk?: { price: number; size: number };
  orderBook: OrderBookRecord;
}

const initState = {
  bidsSeries: [],
  asksSeries: [],
  bestBid: undefined,
  bestAsk: undefined,
  orderBook: {
    buy: new Map<number, number>(),
    sell: new Map<number, number>(),
    timestamp: "",
  },
};

const useCoinbaseTrades = (product: string, aggregation: number) => {
  const [trades, setTrades] = useState<TradesData>(initState);

  const bestBidRef = useRef<{ price: number; size: number }>({
    price: -Infinity,
    size: 0,
  });
  const bestAskRef = useRef<{ price: number; size: number }>({
    price: +Infinity,
    size: 0,
  });

  useEffect(() => {
    setTrades(initState);
    bestBidRef.current = {
      price: -Infinity,
      size: 0,
    };
    bestAskRef.current = {
      price: +Infinity,
      size: 0,
    };

    // TBD: API call to fetch the preview data
  }, [product]);

  useCoinbaseWebSocket(product, aggregation, (bid, ask, orderbook) => {
    // TBD: we have to merge all the orderbooks/ at leasthave them in an array, becase we want to show them combined

    bestBidRef.current =
      bestBidRef.current.price > bid.max.price
        ? { price: bestBidRef.current.price, size: bestBidRef.current.size }
        : { price: bid.max.price, size: bid.max.size };
    
        bestAskRef.current =
      bestAskRef.current.price < ask.min.price
        ? { price: bestAskRef.current.price, size: bestAskRef.current.size }
        : { price: ask.min.price, size: ask.min.size };


    setTrades((prev) => ({
      bidsSeries: [...prev.bidsSeries, { x: bid.timestamp, y: bid.value }],
      asksSeries: [...prev.asksSeries, { x: ask.timestamp, y: ask.value }],
      bestBid: bestBidRef.current,
      bestAsk: bestAskRef.current,
      bidQuantity: 0,
      askQuantity: 0,
      orderBook: orderbook,
    }));
  });

  return trades;
};

export default useCoinbaseTrades;
