import { useEffect, useCallback, useState, useRef } from "react";
import useWebSocket from "react-use-websocket";

const SOCKET_URL = "wss://ws-feed.pro.coinbase.com";

// Indicates time interval that each point on the chart represent, in ms.
const DEFAULT_AGGREGATION_TIME_WINDOW = 1000;

export interface Point {
  min: { price: number; size: number };
  max: { price: number; size: number };
  value: number;
  open: number;
  close: number;
  timestamp: string;
}

export interface OrderBookRecord {
  buy: Map<number, number>;
  sell: Map<number, number>;
  timestamp: string;
}

const register = (
  sendJsonMessage: (message: any) => void,
  selectedProduct: string
) => {
  const subscribeMessage = {
    type: "subscribe",
    product_ids: [selectedProduct],
    channels: ["level2_batch"],
  };

  sendJsonMessage(subscribeMessage);
};

const unregister = (
  sendJsonMessage: (message: any) => void,
  selectedProduct: string
) => {
  const unsubscribeMessage = {
    type: "unsubscribe",
    product_ids: [selectedProduct],
    channels: ["level2_batch"],
  };

  sendJsonMessage(unsubscribeMessage);
};

function shouldAppend(timestamp1: string, timestamp2: string) {
  const date1 = new Date(timestamp1);
  const date2 = new Date(timestamp2);

  const diffInMilliseconds = date2.getTime() - date1.getTime();

  return diffInMilliseconds < DEFAULT_AGGREGATION_TIME_WINDOW;
}

function processSide(timestamp: string, changes: any): Point {
  const open_price = changes[0][1];
  const close_price = changes[changes.length - 1][1];

  // Sort by prices
  changes.sort((a: any, b: any) => a[1] - b[1]);

  const min_price = { price: changes[0][1], size: changes[0][2] };
  const max_price = {
    price: changes[changes.length - 1][1],
    size: changes[changes.length - 1][2],
  };
  const median = changes[Math.floor(changes.length / 2)][1];

  return {
    open: open_price,
    close: close_price,
    min: min_price,
    max: max_price,
    value: median,
    timestamp: timestamp,
  };
}

function processOrderbook(
  timestamp: string,
  changes: any[],
  agg: number
): OrderBookRecord {
  const buy: Map<number, number> = new Map();
  const sell: Map<number, number> = new Map();

  changes.forEach(([side, price, size]) => {
    const priceCategory = Math.round(price / agg) * agg;

    if (side === "buy") {
      if (buy.has(priceCategory)) {
        buy.set(priceCategory, buy.get(priceCategory)! + Number(size));
      } else {
        buy.set(priceCategory, Number(size));
      }
    } else {
      if (sell.has(priceCategory)) {
        sell.set(priceCategory, sell.get(priceCategory)! + Number(size));
      } else {
        sell.set(priceCategory, Number(size));
      }
    }
  });

  return {
    timestamp: timestamp,
    buy: buy,
    sell: sell,
  };
}

const useCoinbaseWebSocket = (
  selectedProduct: string,
  aggregation: number,
  onDataUpdate: (bid: Point, ask: Point, orderbook: OrderBookRecord) => void
) => {
  const changesRef = useRef<any[]>([]);
  const lastTimestampRef = useRef<string>();

  const processMessaage = (timestamp: string, batch: []) => {
    if (lastTimestampRef.current === undefined) {
      lastTimestampRef.current = timestamp;
    }

    if (shouldAppend(lastTimestampRef.current, timestamp)) {
      changesRef.current.push(batch);
      return;
    }

    const data = changesRef.current;
    const buyData = data.filter((c: any[]) => c[0] == "buy");
    const sellData = data.filter((c: any[]) => c[0] == "sell");

    if (buyData.length == 0 || sellData.length == 0) {
      changesRef.current.push(...batch);
      return;
    }

    const changesTimeStamp = lastTimestampRef.current;

    changesRef.current = batch;
    lastTimestampRef.current = timestamp;

    const bid: Point = processSide(changesTimeStamp, buyData);
    const ask: Point = processSide(changesTimeStamp, sellData);
    const orderbook = processOrderbook(changesTimeStamp, data, aggregation);

    onDataUpdate(bid, ask, orderbook);
  };

  const handleWebSocketMessage = useCallback(
    (message: MessageEvent) => {
      const parsedData = JSON.parse(message.data);
      if (parsedData.product_id != selectedProduct) {
        return;
      }

      if (parsedData.type === "l2update") {
        const { time, changes } = parsedData;

        const dataToProcess = changes.filter((d: any) => d[2] > 0);
        if (dataToProcess.length) {
          processMessaage(time, dataToProcess);
        }
      }
    },
    [onDataUpdate]
  );

  const { sendJsonMessage } = useWebSocket(SOCKET_URL, {
    onMessage: handleWebSocketMessage,
    shouldReconnect: () => true,
  });

  useEffect(() => {
    changesRef.current = [];
    lastTimestampRef.current = undefined;

    register(sendJsonMessage, selectedProduct);

    return () => {
      unregister(sendJsonMessage, selectedProduct);
    };
  }, [sendJsonMessage, selectedProduct]);
};

export default useCoinbaseWebSocket;
