import React, { useState } from 'react';
import logo from './logo.svg';
import './App.scss';
import SelectProduct from './SelectProduct/SelectProduct';
import OrderBookChart from './components/OrderBookChart/OrderBookChart';
import OrderBookLadder from './OrderBookLadder/OrderBookLadder';
import useCoinbaseTrades from './Services/TradesHook';

function App() {
  const [selectedProduct, setSelectedProduct] = useState('BTC-USD');
  const [aggregation, setAggregation] = useState(100);

  const trades = useCoinbaseTrades(selectedProduct, aggregation)
  return (
    <div className="App">
        <SelectProduct onProductChange={setSelectedProduct} />
        <OrderBookChart trades={trades} />
        <OrderBookLadder onAggregationChange={setAggregation} trades={trades}/>
    </div>
  );
}

export default App;
